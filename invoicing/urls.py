from rest_framework import routers
from django.urls import path
from . import views
from .api import ProductsViewSet, InvoiceViewSet

router = routers.DefaultRouter()

router.register('products', ProductsViewSet, 'products')
router.register('invoice', InvoiceViewSet, 'invoice')

urlpatterns = router.urls

