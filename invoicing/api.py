from .models import ProductsModel, InvoiceModel
from rest_framework import viewsets, permissions
from .serializers import ProductsSerializer, InvoiceSerializer


class ProductsViewSet(viewsets.ModelViewSet):
    queryset = ProductsModel.objects.all()
    permissions_classes = [permissions.IsAuthenticated]
    serializer_class = ProductsSerializer

class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = InvoiceModel.objects.all()
    permissions_classes = [permissions.IsAuthenticated]
    serializer_class = InvoiceSerializer
