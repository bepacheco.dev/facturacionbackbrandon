from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
# Models
from .models import ProductsModel, InvoiceModel
# Serializaers
from .serializers import ProductsSerializer, InvoiceSerializer

class ProductView(generics.CreateAPIView):
    serializer_class = ProductsSerializer

class InvoiceView(generics.CreateAPIView):
    serializer_class = InvoiceSerializer    
    
