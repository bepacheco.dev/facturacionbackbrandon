from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import ProductsModel, InvoiceModel

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductsModel
        fields = '__all__'

class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceModel
        fields = '__all__'


