from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager


class ProductsModel(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, unique=True)
    price = models.IntegerField()
    stock = models.IntegerField()   


class InvoiceModel(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now_add=True)
    client_name = models.CharField(max_length=255)
    client_direction = models.CharField(max_length=255)
    client_dni = models.CharField(max_length=255,unique=True)
    client_phone = models.CharField(max_length=255)
    seller_name = models.CharField(max_length=255)
    pay_method = models.CharField(max_length=255)
    products =  models.JSONField()
    total = models.IntegerField()



